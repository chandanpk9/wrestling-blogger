// export const BASE_URL =
//   "https://my-json-server.typicode.com/ChandanPk/json-placeholder-api";
// export const BASE_URL = "http://localhost:8001";
export const BASE_URL = "http://ec2-16-171-6-182.eu-north-1.compute.amazonaws.com:8001";

export const API = {
  blogs: BASE_URL + "/blogs",
};

API.blogPage = (id) => BASE_URL + "/blogs/" + id;
