import mongoose from "mongoose";

const ticketSchema = new mongoose.Schema({
  user_email: {
    type: String,
    required: true,
  },

  title: {
    type: String,
    required: true,
  },

  priority: {
    type: String,
    required: true,
  },

  body: {
    type: String,
    required: true,
  },
});

const Ticket = mongoose.model("ticket", ticketSchema);

export { Ticket };
